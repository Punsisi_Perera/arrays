#include <stdio.h>
#include <string.h>

int main()
{
  	char sentence[100];
  	int len, begin, end;

  	printf(" Enter any String :  ");
  	gets(sentence);

  	len = strlen(sentence);
  	end = len - 1;

  	printf("String in Reverse Order : ");
  	for(int i = len - 1; i >= 0; i--)
	{
		if(sentence[i] == ' ' || i == 0)
		{
			if(i == 0)
			{
				begin = 0;
			}
			else
			{
				begin = i + 1;
			}
			for(int j = begin; j <= end; j++)
			{
				printf("%c", sentence[j]);
			}
			end = i - 1;
			printf(" ");
		}
	}

  	return 0;
}
